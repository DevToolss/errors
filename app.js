var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var neo4j = require('neo4j-driver');
var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

let driver = neo4j.driver('bolt://localhost', neo4j.auth.basic('neo4j', '123'));
let session = driver.session();


app.get('/', function(req, res) {
    session
    .run('MATCH(n:Local) return n LIMIT 25')
    .then(function(result) {

        let translateArray = [];
        
        result.records.forEach(function(record) {

            translateArray.push({
                
                id: record._fields[0].properties.low,

                text: record._fields[0].properties.text,
                name: record._fields[0].properties.name,
                code: record._fields[0].properties.code,
            });
        });
        res.render('index', {
            translates: translateArray
        });
    })
    .catch(function(err) {
        console.log(err);
    });
})

app.post('/errors/add', function(req,res) {

    let e = req.body.errName;
    session
        .run('create (n:Error {name: $ErrName}) return n', {ErrName:e})
        .then(function(result) {
            
            session.close();
        })
        .catch(function(err) {
            console.log(err)
        })
    res.redirect('/');
});

app.post('/lang/add', function(req,res) {
    
    let c = req.body.countryCode;
    let e = req.body.err;
    let t = req.body.errTranslate;

    session
        .run('create (n:Local {code: $c, name: $e, text: $t})', {e: e, c: c, t: t})

        .then(function(result) {
           
            session.close();
        })
        .catch(function(err) {
            console.log(err)
        })
    res.redirect('/');
});

app.post('/lang_and_err/add', function(req,res) {

    let e = req.body.err;
    session
        .run('MATCH (a:Error {name: $name}), (b: Local {name: $name}) MERGE (a)-[r:Has]->(b)',{name:e})

        .then(function(result) {
           
            session.close();
        })
        .catch(function(err) {
            console.log(err)
        })
    res.redirect('/');
});

app.post('/remove/translate', function(req, res) {

    let e = req.body.err;
    let t = req.body.translate;
    session
        .run('MATCH (a:Error {name: $name}), (b: Local {name: $name}) MERGE (a)-[r:Has]->(b) delete r',{name:e})

        .then(function(result)
         {
            session
            .run('MATCH(n {name: $name}) delete n', {name: e})
            .then(function(result)
            {
              
            })
            .catch(function(err) 
            {
                console.log(err);
            })
        })
        .catch(function(err) {
            console.log(err)
        })
    res.redirect('/');
})

app.listen(3000);
console.log('3000 port');

module.exports = app;